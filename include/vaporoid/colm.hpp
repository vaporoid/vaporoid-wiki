#ifndef VAPOROID_COLM_HPP
#define VAPOROID_COLM_HPP

#include <colm/colm.h>
#include <colm/program.h>
#include <string>
#include <vector>
#include <boost/foreach.hpp>

extern RuntimeData colm_object;

namespace vaporoid {
  namespace colm {
    inline struct colm_program* new_program() {
      return colm_new_program(&colm_object);
    }

    inline void run_program(struct colm_program* program, const std::vector<std::string> args) {
      std::vector<const char*> argv;
      argv.reserve(args.size() + 1);
      BOOST_FOREACH(const std::string& i, args) {
        argv.push_back(i.c_str());
      }
      argv.push_back(0);
      colm_run_program(program, args.size(), &argv[0]);
    }
  }
}

#endif
