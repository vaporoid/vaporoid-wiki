#include <lua.hpp>
#include <stdlib.h>
#include <iostream>
#include <string>

void run(const std::string& path) {
  std::cerr << "p0:" << std::endl;
  lua_State* L = luaL_newstate();
  std::cerr << "p1:" << L << std::endl;
  luaL_openlibs(L);
  std::cerr << "p2:" << L << std::endl;
  luaL_dofile(L, path.c_str());
  std::cerr << "p3:" << L << std::endl;
  lua_close(L);
  std::cerr << "p4:" << L << std::endl;
}

int main(int argc, char* argv[]) {
  int size = atoi(argv[1]);
  std::cerr << size << std::endl;
  for (int i = 0; i < size; ++i) {
    run(argv[2]);
  }
  return 0;
}
