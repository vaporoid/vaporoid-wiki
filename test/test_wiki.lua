#! /usr/bin/env lua

local vaporoid = {
  colm = require("vaporoid.colm");
  wiki = require("vaporoid.wiki");
}
local swig = require("vaporoid_wiki")

vaporoid.wiki.text_generator = require("vaporoid.wiki.text_generator")

local function array_writer ()
  return setmetatable({
    out = {};

    write = function (self, ...)
      for i, v in ipairs({...}) do
        table.insert(self.out, v)
      end
    end;
  }, {
    __tostring = function (self)
      return table.concat(self.out)
    end;
  })
end

local function test1 ()
  -- must compile with -fno-toplevel-reorder
  local program = vaporoid.colm.program(swig)
  program:run("text", "** //**")
  assert(program.Start.__tree == nil)
  io.stderr:write(string.format("[test1] %s\n", program.Error:text()))
  program:delete()
end

local function test2 ()
  local program = vaporoid.colm.program(swig)
  program:run("text", "[[ http://wwww.example.com/ | {{ example.png | example }} ]]")
  io.stderr:write(string.format("[test2] %s\n", program.Start:text()))
  program:delete()
end

local function test3 ()
  local program = vaporoid.colm.program(swig)
  program:run("text", [=[
foo ** b // bi // ** // i ** ib ** // <bar>
{{example.png|example}}
[[Wikipedia:日本語]]
[[http://www.example.com/|example]]
~** foo
\\
**//[[http://www.example.com/|{{example.png|example}}]]//**]=])
  io.write("<p>")
  vaporoid.wiki.text_generator(io.stdout):generate(vaporoid.colm(program.Start).Text)
  io.write("</p>\n")
  program:delete()
end

local function test4 ()
  vaporoid.wiki.run(io.stdout, [=[
= foo
== bar = = = =
=== baz  ===  
* 1
** 2
*** 3
** foo
** bar
* baz
** qux
# x
## y
----
### 3
foo bar
foo ---- bar
foo | bar
|= foo |= bar |
| baz | qux |
| foobar | baz
| {{foo|bar}} | baz |
}}}

foo
  bar
    baz


{{{
if (true) {
  if (true) {
    if (true) {
    fuck();
 }}}
}}}

foo bar
** // invalid **
baz qux

foo \\ bar
]=])
end

local function test5 ()
  local out = array_writer()
  vaporoid.wiki.run(out, "&=&")
  io.write(tostring(out), "\n")
  assert(tostring(out):find("&amp;=&amp;"))
end

local function test6 ()
  local out = array_writer()
  vaporoid.wiki.run(out, [=[
[[Google:英語&日本語]]
[[Twitter:vaporoid|@vaporoid]]
[[NoSuchName:foo&bar]]
]=])
  io.write(tostring(out), "\n")
  assert(tostring(out):find("q=%%"))
end

local function test7 ()
  local out = array_writer()
  vaporoid.wiki.run(out, [=[
; First title of definition list
: Definition of first item.
; Second title: Second definition
beginning on the same line.
]=])
  io.write(tostring(out), "\n")
end

local function test8 ()
  local out = array_writer()
  vaporoid.wiki.run(out, [=[
This is a normal paragraph.
> This is an indented
paragraph in two lines.
>> This is more indented.
]=])
  io.write(tostring(out), "\n")
end


local function test (n)
  for i = 1, n do
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    test7()
    test8()
  end
end

local n, wait = ...
n = n == nil and 1 or tonumber(n)

test(n)

print(collectgarbage("count"))
collectgarbage()
print(collectgarbage("count"))
collectgarbage()
print(collectgarbage("count"))

if wait ~= nil then
  io.stdout:write("? ")
  io.stdin:read("*l")
end
