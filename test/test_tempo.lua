#! /usr/bin/env lua

local vaporoid = require("vaporoid")

local function test1()
  local compiler = vaporoid.tempo.compiler()
  compiler:run([[
[% IF foo.bar.baz %]
"foo": [% FOO.BAR | fn.escape_html %]
[% ELSEIF bar %]
"bar": [% x42 %]
[% ELSE %]
otherwise
[% END %]
[% WHILE it %]
  foo!
[% END %]
]])

  local metatable = {
    __index = function (self, key)
      if rawget(self, key .. "_") then
        self[key .. "_"] = self[key .. "_"] + 1
        return self[key .. "_"] <= 4
      end
      return nil
    end;
  }

  local ctx = vaporoid.tempo.context(io.stdout, setmetatable({
    it_ = 0;
    foo = {
      bar = {
        baz = false;
      };
    };
    bar = true;
    x42 = "日本語";
    FOO = {
      BAR = "<html>";
    };
    fn = {
      escape_html = vaporoid.html.escape;
    };
  }, metatable))

  vaporoid.tempo.machine(compiler.code, ctx):run()
end

local function test (n)
  for i = 1, n do
    test1()
  end
end

local n, wait = ...
n = n == nil and 1 or tonumber(n)

test(n)

print(collectgarbage("count"))
collectgarbage()
print(collectgarbage("count"))
collectgarbage()
print(collectgarbage("count"))

if wait ~= nil then
  io.stdout:write("? ")
  io.stdin:read("*l")
end
