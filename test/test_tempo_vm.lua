#! /usr/bin/env lua

local vaporoid = {
  html = require("vaporoid.html");
  tempo = {
    context = require("vaporoid.tempo.context");
    opcode = require("vaporoid.tempo.opcode");
    machine = require("vaporoid.tempo.machine");
  };
}

--[[
if flag then
  write("foo\n")
else
  write("bar\n")
end
write("baz\n")
write(a)
write("\n")
b = 105
write(b)
write("\n")
write(foo.bar.baz)
write(test.html | test.escape_html)
]]

local opcode = vaporoid.tempo.opcode
local code = {
  { opcode.ENV };
  { opcode.GET, "flag" };
  { opcode.NOT };
  { opcode.IF, 7 };
  { opcode.PUSH, "foo\n" };
  { opcode.WRITE };
  { opcode.GOTO, 9 };
  { opcode.PUSH, "bar\n" };
  { opcode.WRITE };
  { opcode.PUSH, "baz\n" };
  { opcode.WRITE };
  { opcode.ENV };
  { opcode.GET, "a" };
  { opcode.WRITE };
  { opcode.PUSH, "\n" };
  { opcode.WRITE };
  { opcode.ENV };
  { opcode.PUSH, 105 };
  { opcode.PUT, "b" };
  { opcode.ENV };
  { opcode.GET, "b" };
  { opcode.WRITE };
  { opcode.PUSH, "\n" };
  { opcode.WRITE };
  { opcode.ENV };
  { opcode.GET, "foo" };
  { opcode.GET, "bar" };
  { opcode.GET, "baz" };
  { opcode.WRITE };
  { opcode.ENV };
  { opcode.GET, "test" };
  { opcode.GET, "escape_html" };
  { opcode.ENV };
  { opcode.GET, "test" };
  { opcode.GET, "html" };
  { opcode.CALL, 2 };
  { opcode.ENV };
  { opcode.GET, "write" };
  { opcode.SWAP, 0, 1 };
  { opcode.CALL, 2 };
}

local ctx = vaporoid.tempo.context(io.stdout, {
  flag = true;
  a = 42;
  b = 69;
  foo = {
    bar = {
      baz = "qux\n";
    };
  };
  test = {
    html = "<html>";
    escape_html = vaporoid.html.escape;
  };
  write = function (...)
    for i,v in ipairs({...}) do
      io.write(v)
    end
  end;
})

vaporoid.tempo.machine(code, ctx):run()
