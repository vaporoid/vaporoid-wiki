#! /bin/sh -e

for i in data/*.txt
do
  name=`expr "x$i" : 'x.*/\(.*\)\.'`
  out="$CMAKE_CURRENT_BINARY_DIR/$name.html"

  echo '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>*</title></head><body>' >"$out"
  lua -e 'require("vaporoid").wiki.run(io.stdout, io.read("*a"))' <"$i" >>"$out"
  echo '</body></html>' >>"$out"
done
