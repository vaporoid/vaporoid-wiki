PRAGMA foreign_keys = ON;
PRAGMA case_sensitive_like = ON;

CREATE TABLE test (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  word TEXT,
  -- word TEXT UNIQUE
  -- word TEXT UNIQUE COLLATE BINARY
  -- word TEXT UNIQUE COLLATE NOCASE
);
-- CREATE INDEX index_test_word ON test (word COLLATE NOCASE);

-- .timer ON
-- PRAGMA case_sensitive_like = ON;
