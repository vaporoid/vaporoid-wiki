local class_a
local class_b

local self_metatable = {
  __index = function (self, key)
    return self.class[key] or self.super and self.super[key]
  end;
}

local class_metatable = {
  __call = function (self, ...)
    return self:new(...)
  end;
}

do
  local class = {}

  function class:new (...)
    local self = {
      class = self;
    }
    return setmetatable(self, self_metatable)
  end

  function class:foo (value)
    print("a/foo: ", value)
  end

  function class:bar (value)
    print("a/bar: ", value)
  end

  class_a = setmetatable(class, class_metatable)
end

do
  local class = {}

  function class:new (...)
    local self = {
      class = self;
    }
    return setmetatable(self, self_metatable)
  end

  function class:foo (value)
    if self.super then
      self.super:foo(value)
    end
    print("b/foo: ", value)
  end

  function class:baz (value)
    print("b/baz: ", value)
  end

  class_b = setmetatable(class, class_metatable)
end

local a = class_a()
local b = class_b()
b.super = a
a:foo("1")
b:foo("2")
b:bar("3")
b:baz("4")


