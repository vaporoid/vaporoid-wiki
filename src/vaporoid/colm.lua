local class = {
  node    = require("vaporoid.colm.node");
  program = require("vaporoid.colm.program");
}

local function iterator (s, var)
  if var.node["end"](var.node) == 0 then
    return var.next, var.value
  else
    return nil
  end
end

function class.iterator (node)
  return iterator, nil, node
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.node(...)
  end;
})
