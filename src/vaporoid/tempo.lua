local class = {
  compiler = require("vaporoid.tempo.compiler");
  context  = require("vaporoid.tempo.context");
  machine  = require("vaporoid.tempo.machine");
}

function class.compile (text)
  local compiler = class.compiler()
  compiler:run(text)
  return compiler.code
end

function class.run (code, ...)
  class.machine(code, class.context(...)):run()
end

return class
