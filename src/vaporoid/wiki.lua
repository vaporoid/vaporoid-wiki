local class = {
  generator = require("vaporoid.wiki.generator");
  interwiki = require("vaporoid.wiki.interwiki");
  parser    = require("vaporoid.wiki.parser");
}

function class.parse (text)
  local parser = class.parser()
  parser:parse(text)
  return parser.data
end

function class.generate (...)
  local generator = class.generator(...)
  generator:generate()
end

function class.run (out, text, ext)
  class.generate(out, class.parse(text), ext)
end

return class
