local class = {}

local escape = {
  ["&"]  = "&amp;";
  ["<"]  = "&lt;";
  [">"]  = "&gt;";
  ["\""] = "&quot;";
}

function class.escape (s)
  return (s:gsub("[&<>\"]", escape))
end

return class
