local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key] or self.cache[key]
end

local class = {}

function class.new (class)
  return setmetatable({
    class  = class;
    data   = {};
    nowiki = false;
    cache  = {};
  }, metatable)
end

function class:match (s, pattern)
  self.cache = { s:match(pattern) }
  return self[1]
end

function class:push (name, text)
  local back = self.data[#self.data] or {}
  if name == back.name then
    table.insert(back.text, text)
  else
    table.insert(self.data, { name = name; text = { text } })
  end
end

function class:push_list (name, text)
  local back = self.data[#self.data] or {}
  local level = self[1]:len()
  if name == back.name then
    if level < back.level + 2 then
      back.level = level
      local list = back.list
      for j = 2, level do
        list = list[#list].list
      end
      table.insert(list, { text = { self[2] }; list = {} })
    else
      self:push("p", text)
    end
  else
    if level == 1 then
      table.insert(self.data, { name = name; level = level; list = { { text = { self[2] }; list = {} } } })
    else
      self:push("p", text)
    end
  end
end

function class:push_text (text)
  local back = self.data[#self.data] or {}
  if back.name == "ul" or back.name == "ol" or back.name == "dl" or back.name == "blockquote" then
    local list = back.list
    for j = 2, back.level do
      list = list[#list].list
    end
    table.insert(list[#list].text, text)
  else
    self:push("p", text)
  end
end

function class:parse_line (line)
  if self.nowiki then
    if line == "}}}" then
      self.nowiki = false
    elseif self:match(line, "^%s(%s*}}}%s*)$") then
      self:push("nowiki", self[1])
    else
      self:push("nowiki", line)
    end
  elseif self:match(line, "^%s*(=+)%s*(.-)%s*=*%s*$") then
    table.insert(self.data, { name = "h"; level = self[1]:len(); text = { self[2] } })
  elseif self:match(line, "^%s*(%*+)%s*(.*)") then
    self:push_list("ul", line)
  elseif self:match(line, "^%s*(#+)%s*(.*)") then
    self:push_list("ol", line)
  elseif self:match(line, "^%s*(;)%s*(.*)") then
    self:push_list("dl", line)
  elseif self:match(line, "^%s*(>+)%s*(.*)") then
    self:push_list("blockquote", line)
  elseif line:find("^%s*%-%-%-%-%s*$") == 1 then
    table.insert(self.data, { name = "hr" })
  elseif self:match(line, "^%s*(|.-)%s*|?%s*$") then
    self:push("table", self[1])
  elseif line == "{{{" then
    self.nowiki = true
  elseif line:find("^%s*$") == 1 then
    self:push("blank")
  else
    self:push_text(line)
  end
end

function class:parse (text)
  for i in text:gmatch("([^\r\n]*)\r?\n?") do
    self:parse_line(i)
  end
end

return setmetatable(class, {
  __call = function (class, ...)
    return class:new(...)
  end;
})
