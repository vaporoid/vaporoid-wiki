local vaporoid = {
  html = require("vaporoid.html");
  uri  = require("vaporoid.uri");
}

local function encode_uri (node)
  return vaporoid.html.escape(vaporoid.uri.encode(tostring(node)))
end

local function encode_uri_component (node)
  return vaporoid.uri.encode_component(tostring(node))
end

--------------------------------------------------------------------------------

local class = {}

function class.Google (out, title)
  out:write("<a href=\"https://www.google.com/search?q=", encode_uri_component(title), "\">");
end

function class.Twitter (out, title)
  out:write("<a href=\"https://twitter.com/", encode_uri(title), "\">");
end

function class.WikipediaEn (out, title)
  out:write("<a href=\"http://en.wikipedia.org/wiki/", encode_uri(title), "\">");
end

function class.WikipediaJa (out, title)
  out:write("<a href=\"http://ja.wikipedia.org/wiki/", encode_uri(title), "\">");
end

function class.Eow (out, title)
  out:write("<a href=\"http://eow.alc.co.jp/search?q=", encode_uri_component(title), "\">");
end

return class
