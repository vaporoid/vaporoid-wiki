local vaporoid = {
  colm = require("vaporoid.colm");
  html = require("vaporoid.html");
  uri  = require("vaporoid.uri");
  wiki = {
    interwiki = require("vaporoid.wiki.interwiki");
  };
}

local function sub (node, ...)
  return vaporoid.html.escape(tostring(node):sub(...))
end

local function escape_html (node)
  return vaporoid.html.escape(tostring(node))
end

local function encode_uri (node)
  if node.Uri then
    return vaporoid.html.escape(tostring(node.Uri))
  else
    return vaporoid.html.escape(vaporoid.uri.encode(tostring(node)))
  end
end

--------------------------------------------------------------------------------

local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key]
end

--------------------------------------------------------------------------------

local class = {}

function class.new (out, ext)
  return setmetatable({
    class = class;
    out   = out;
    ext   = ext;
  }, metatable)
end

function class:write (...)
  self.out:write(...)
end

function class:generate_nowiki (node)
  self:write("<code>", sub(node.Nowiki_Open, 4), escape_html(node.Nowiki_Text), sub(node.Nowiki_Close, 4), "</code>")
end

function class:generate_image (node)
  self:write("<img class=\"img-polaroid\" src=\"", encode_uri(node.Image1_Text), "\"")
  if node.Image2_Text then
    self:write(" alt=\"", escape_html(node.Image2_Text), "\"")
  end
  self:write(">")
end

function class:generate_link (node)
  if node.Link2_Text then
    if vaporoid.wiki.interwiki[tostring(node.Link1_Text)] then
      vaporoid.wiki.interwiki[tostring(node.Link1_Text)](self.out, node.Link2_Text)
    else
      self:write("<a href=\"", encode_uri(node.Link1_Text), ":", encode_uri(node.Link2_Text), "\">")
    end
    if node.Link3_Text then
      self:generate(node.Link3_Text)
    else
      self:write(escape_html(node.Link1_Text), ":", escape_html(node.Link2_Text))
    end
  else
    self:write("<a href=\"", encode_uri(node.Link1_Text), "\">")
    if node.Link3_Text then
      self:generate(node.Link3_Text)
    else
      self:write(escape_html(node.Link1_Text))
    end
  end
  self:write("</a>")
end

function class:generate_inline (name, node)
  self:write("<", name, ">")
  self:generate(node)
  self:write("</", name, ">")
end

function class:generate_placeholder (node)
  self:write("<!-- unknown placeholder: ", escape_html(node.Placeholder_Text), " -->")
end

function class:generate_plugin(node)
  local lua = tostring(node.Plugin_Text):match("%s*#! lua%s+(.*)")
  if lua then
    local result, message = pcall(load(lua), self.ext)
    if not result then
      self:write("<!-- lua plugin error: ", escape_html(message), " -->")
    end
  else
    self:write("<!-- unknown plugin: ", escape_html(node.Plugin_Text), " -->")
  end
end

function class:generate_char (node)
  if node.Nowiki then
    self:generate_nowiki(node.Nowiki)
  elseif node.Image then
    self:generate_image(node.Image)
  elseif node.Link then
    self:generate_link(node.Link)
  elseif node.Bold then
    self:generate_inline("strong", node.Text)
  elseif node.Italic then
    self:generate_inline("em", node.Text)
  elseif node.Monospace then
    self:generate_inline("code", node.Text)
  elseif node.Superscript then
    self:generate_inline("sup", node.Text)
  elseif node.Subscript then
    self:generate_inline("sub", node.Text)
  elseif node.Underline then
    self:generate_inline("u", node.Text)
  elseif node.Escape then
    self:write(sub(node.Escape, 2))
  elseif node.Line_Break then
    self:write("<br>")
  elseif node.Url then
    self:write("<a href=\"", node.Url, "\">", escape_html(node.Url), "</a>")
  elseif node.Wiki_Word then
    if self.ext and self.ext.wiki_word == false then
      self:write(escape_html(node.Wiki_Word))
    else
      self:write("<a href=\"", encode_uri(node.Wiki_Word), "\">", escape_html(node.Wiki_Word), "</a>")
    end
  elseif node.Placeholder then
    self:generate_placeholder(node.Placeholder)
  elseif node.Plugin then
    self:generate_plugin(node.Plugin)
  elseif node.Char then
    if tostring(node.Char) == "\n" and self.ext and self.ext.newline then
      self:write(self.ext.newline)
    else
      self:write(escape_html(node.Char))
    end
  else
    error("invalid node: " .. node)
  end
end

function class:generate (node)
  for i, v in vaporoid.colm.iterator(node) do
    self:generate_char(v)
  end
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
