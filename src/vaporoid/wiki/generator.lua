local vaporoid = {
  colm   = require("vaporoid.colm");
  html   = require("vaporoid.html");
  uri    = require("vaporoid.uri");
  writer = require("vaporoid.writer");
  wiki = {
    text_generator = require("vaporoid.wiki.text_generator");
  };
}
local swig = require("vaporoid_wiki")

local function text (text)
  return table.concat(text, "\n")
end

local function escape_html (node)
  return vaporoid.html.escape(tostring(node))
end

local function encode_uri_component (node)
  return vaporoid.uri.encode_component(tostring(node))
end

local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key]
end

local class = {}

function class.new (class, out, data, ext)
  return setmetatable({
    class = class;
    out   = out;
    data  = data;
    ext   = ext;
  }, metatable)
end

function class:write (...)
  self.out:write(...)
end

function class:is_block_plugin (node)
  for i, v in vaporoid.colm.iterator(node) do
    if not v.Plugin then
      return false
    end
  end
  return true
end

function class:generate_text_node (node)
  vaporoid.wiki.text_generator(self.out, self.ext):generate(node)
end

function class:generate_text (text, is_paragraph)
  local program = vaporoid.colm.program(swig)
  program:run("text", text)
  if program.Start.__tree then
    local node = vaporoid.colm(program.Start).Text
    if is_paragraph then
      if self:is_block_plugin(node) then
        self:generate_text_node(node)
      else
        self:write("<p>")
        self:generate_text_node(node)
        self:write("</p>")
      end
    else
      self:generate_text_node(node)
    end
  else
    self:write("<span class=\"text-error\">", escape_html(program.Error:text()), "</span>")
  end
  program:delete()
end

function class:generate_list (name, list)
  if #list > 0 then
    self:write("<", name, ">")
    for i, v in ipairs(list) do
      self:write("<li>")
      self:generate_text(text(v.text))
      self:generate_list(name, v.list)
      self:write("</li>")
    end
    self:write("</", name, ">")
  end
end

function class:generate_dl (list)
  if #list > 0 then
    self:write("<dl>")
    for i, v in ipairs(list) do
      local program = vaporoid.colm.program(swig)
      program:run("di", text(v.text))
      if program.Start_Di.__tree then
        local node = vaporoid.colm(program.Start_Di)
        self:write("<dt>")
        self:generate_text_node(node.Dt_Text)
        self:write("</dt><dd>")
        self:generate_text_node(node.Dd_Text)
        self:write("</dd>")
      else
        self:write("<dt><span class=\"text-error\">")
        self:write(escape_html(program.Error:text()))
        self:write("</span></dt><dd></dd>")
      end
      program:delete()
    end
    self:write("</dl>")
  end
end

function class:generate_blockquote (list)
  if #list > 0 then
    self:write("<blockquote>")
    for i, v in ipairs(list) do
      self:write("<p>")
      self:generate_text(text(v.text))
      self:write("</p>")
      self:generate_blockquote(v.list)
    end
    self:write("</blockquote>")
  end
end

function class:generate_cell (node)
  if node.Th then
    self:write("<th>")
    self:generate_text_node(node.Text)
    self:write("</th>")
  elseif node.Td then
    self:write("<td>")
    self:generate_text_node(node.Text)
    self:write("</td>")
  else
    error("invalid node: " .. node)
  end
end

function class:generate_tr (node)
  local column = 0
  for i, v in vaporoid.colm.iterator(node) do
    self:generate_cell(v)
    column = column + 1
  end
  return column
end

function class:generate_table (text)
  local buffer = {}
  local column = 1

  for i, v in ipairs(text) do
    local out = self.out
    self.out = vaporoid.writer.array()

    buffer[i] = { column = 1 }

    local program = vaporoid.colm.program(swig)
    program:run("tr", v)
    if program.Start_Tr.__tree then
      buffer[i].column = self:generate_tr(vaporoid.colm(program.Start_Tr).Tr)
    else
      self:write("<td><span class=\"text-error\">")
      self:write(escape_html(program.Error:text()))
      self:write("</span></td>")
    end
    program:delete()

    buffer[i].data = table.concat(self.out.out)
    self.out = out

    column = math.max(column, buffer[i].column)
  end

  self:write("<table class=\"table table-striped table-bordered\">")
  for i, v in ipairs(buffer) do
    self:write("<tr>", v.data)
    for i = v.column, column - 1 do
      self:write("<td></td>")
    end
    self:write("</tr>")
  end
  self:write("</table>")
end

function class:generate_data (data)
  if data.name == "h" then
    local text = text(data.text)
    self:write("<h", data.level, ">", escape_html(text), "</h", data.level, ">")
  elseif data.name == "ul" then
    self:generate_list("ul", data.list)
  elseif data.name == "ol" then
    self:generate_list("ol", data.list)
  elseif data.name == "dl" then
    self:generate_dl(data.list)
  elseif data.name == "blockquote" then
    self:generate_blockquote(data.list)
  elseif data.name == "hr" then
    self:write("<hr>")
  elseif data.name == "table" then
    self:generate_table(data.text)
  elseif data.name == "nowiki" then
    self:write("<pre>", escape_html(text(data.text)), "</pre>")
  elseif data.name == "p" then
    self:generate_text(text(data.text), true)
  elseif data.name == "blank" then
    return
  else
    error("invalid data: " .. data.name)
  end
end

function class:generate ()
  for i, v in ipairs(self.data) do
    self:generate_data(v)
  end
end

return setmetatable(class, {
  __call = function (class, ...)
    return class:new(...)
  end;
})
