local function push (stack, value)
  table.insert(stack, value)
end

local function pop (stack)
  return table.remove(stack)
end

local class = {}

function class.NOP (context)
  -- noop
end

function class.ENV (context)
  push(context.stack, context.env)
end

function class.DUP (context)
  push(context.stack, context.stack[#context.stack])
end

function class.PUSH (context, value)
  push(context.stack, value)
end

function class.POP (context)
  pop(context.stack)
end

function class.SWAP (context, m, n)
  local size = #context.stack
  local m = size - m
  local n = size - n
  context.stack[m], context.stack[n] = context.stack[n], context.stack[m]
end

function class.GOTO (context, address)
  context.pc = address
end

function class.IF (context, address)
  if pop(context.stack) then
    context.pc = address
  end
end

function class.GET (context, key)
  local size = #context.stack
  context.stack[size] = context.stack[size][key]
end

function class.PUT (context, key)
  local value = pop(context.stack)
  context.stack[#context.stack][key] = value
end

function class.NOT (context)
  local size = #context.stack
  context.stack[size] = not context.stack[size]
end

function class.WRITE (context)
  context:write(pop(context.stack))
end

function class.CALL (context, argc)
  local argv = {}
  for i = 1, argc do
    table.insert(argv, 1, pop(context.stack))
  end
  push(context.stack, argv[1](table.unpack(argv, 2)))
end

return class
