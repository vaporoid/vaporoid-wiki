local vaporoid = {
  colm = require("vaporoid.colm");
  tempo = {
    opcode = require("vaporoid.tempo.opcode");
  };
}
local swig = require("vaporoid_tempo")

local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key]
end

local class = {}

function class.new ()
  return setmetatable({
    class = class;
    code  = {};
    label = 0;
  }, metatable)
end

function class:push (...)
  table.insert(self.code, {...})
end

function class:generate_label ()
  local label = "L" .. self.label
  self.label = self.label + 1
  return label
end

function class:generate_member (node)
  if node.Member then
    self:generate_member(node.Member)
  else
    self:push("ENV")
  end
  self:push("GET", tostring(node.Identifier))
end

function class:generate_filter (node)
  self:generate_member(node.Member2)
  if node.Member1 then
    self:generate_member(node.Member1)
  elseif node.Filter1 then
    self:generate_filter(node.Filter1)
  else
    error("invalid node: " .. node)
  end
  self:push("CALL", 2)
end

function class:generate_expression (node)
  if node.Member then
    self:generate_member(node.Member)
  elseif node.Filter then
    self:generate_filter(node.Filter)
  else
    error("invalid node: " .. node)
  end
end

function class:generate_elseif (node, label_end)
  local label = self:generate_label()
  self:generate_expression(node.Condition)
  self:push("NOT")
  self:push("IF", label)
  self:generate(node.Statement)
  self:push("GOTO", label_end)
  self:push("NOP", label)
end

function class:generate_else (node)
  self:generate(node.Statement)
end

function class:generate_statement (node)
  if node.If then
    local label = self:generate_label()
    local label_end = self:generate_label()
    self:generate_expression(node.Condition)
    self:push("NOT")
    self:push("IF", label)
    self:generate(node.Statement)
    self:push("GOTO", label_end)
    self:push("NOP", label)
    if node.Elseif then
      for i, v in vaporoid.colm.iterator(node.Elseif) do
        self:generate_elseif(v, label_end)
      end
    end
    if node.Else then
      self:generate_else(node.Else)
    end
    self:push("NOP", label_end)
  elseif node.While then
    local label = self:generate_label()
    local label_end = self:generate_label()
    self:push("NOP", label)
    self:generate_expression(node.Condition)
    self:push("NOT")
    self:push("IF", label_end)
    self:generate(node.Statement)
    self:push("GOTO", label)
    self:push("NOP", label_end)
  elseif node.Write then
    self:generate_expression(node.Write)
    self:push("WRITE")
  elseif node.Text then
    self:push("PUSH", tostring(node.Text):gsub(">%s+<", "><"))
    self:push("WRITE")
  else
    error("invalid node: " .. node)
  end
end

function class:generate (node)
  for i, v in vaporoid.colm.iterator(node) do
    self:generate_statement(v)
  end
end

function class:translate ()
  local map = {}
  for i, v in ipairs(self.code) do
    if v[1] == "NOP" and v[2] then
      map[v[2]] = i
      table.remove(v)
    end
  end
  for i, v in ipairs(self.code) do
    if v[1] == "GOTO" or v[1] == "IF" then
      v[2] = map[v[2]]
    end
    v[1] = vaporoid.tempo.opcode[v[1]]
  end
end

function class:run (text)
  local message
  local program = vaporoid.colm.program(swig)
  program:run("%]" .. text:gsub("^%s+", ""):gsub("%s+$", "") .. "[%")
  if program.Start.__tree then
    self:generate(vaporoid.colm(program.Start).Statement)
  else
    message = program.Error:text()
  end
  program:delete()
  if message then
    error(message)
  end
  self:translate()
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
