local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key]
end

local class = {}

function class.new (code, context)
  return setmetatable({
    class   = class;
    code    = code;
    context = context;
  }, metatable)
end

function class:run ()
  local code    = self.code
  local context = self.context

  while context.pc <= #code do
    code[context.pc][1](context, table.unpack(code[context.pc], 2))
    context.pc = context.pc + 1
  end
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
