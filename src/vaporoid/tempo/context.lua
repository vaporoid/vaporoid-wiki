local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key]
end

local class = {}

function class.new (out, env)
  return setmetatable({
    class = class;
    out   = out;
    env   = env;
    pc    = 1;
    stack = {};
  }, metatable)
end

function class:write (...)
  self.out:write(...)
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
