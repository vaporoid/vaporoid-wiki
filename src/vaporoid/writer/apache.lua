local metatable = {}

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key]
end

local class = {}

function class.new (out)
  return setmetatable({
    class = class;
    out   = out;
  }, metatable)
end

function class:write (...)
  self.out:puts(...)
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
