local metatable = {}

local function cache (self, key)
  if self.cache[key] == nil then
    if self.swig[key] then
      local value = self.swig[key](self.program)
      if value then
        self.cache[key] = value
        return value
      end
    end
    self.cache[key] = false
    return nil
  end
  return self.cache[key] and self.cache[key] or nil
end

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key] or cache(self, key)
end

local class = {}

function class.new (swig)
  local self = {
    class   = class;
    swig    = swig;
    program = swig.new_program();
    cache   = {};
  }
  return setmetatable(self, metatable)
end

function class:run (...)
  local argv = self.swig[self.swig.prefix() .. "vector_string"]()
  for i, v in ipairs({...}) do
    argv:push_back(v)
  end
  self.swig.run_program(self.program, argv)
end

function class:delete (...)
  self.swig.colm_delete_program(self.program)
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
