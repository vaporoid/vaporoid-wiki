local metatable = {}

local function cache (self, key)
  if self.cache[key] == nil then
    if self.node[key] then
      local node = self.node[key](self.node)
      if node.__tree then
        local value = self.class(node)
        self.cache[key] = value
        return value
      end
    end
    self.cache[key] = false
    return nil
  end
  return self.cache[key] and self.cache[key] or nil
end

function metatable:__index (key)
  return self.class[key] or rawget(self, "super") and self.super[key] or cache(self, key)
end

function metatable:__tostring ()
  if not self.cache.text then
    local value = tostring(self.node:text())
    self.cache.text = value
    return value
  end
  return self.cache.text
end

local class = {}

function class.new (node)
  local self = {
    class = class;
    node  = node;
    cache = {};
  }
  return setmetatable(self, metatable)
end

return setmetatable(class, {
  __call = function (class, ...)
    return class.new(...)
  end;
})
