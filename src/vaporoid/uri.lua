-- http://www.ietf.org/rfc/rfc3986.txt
local class = {}

local function encode (s)
  return string.format("%%%02X", s:byte())
end

-- escapes all characters except unreserved, gen_delims and sub_delims
function class.encode (s)
  return (s:gsub("[^A-Za-z0-9%-._~:/?#[%]@!$&'()*+,;=]", encode))
end

-- escapes all characters except unreserved
function class.encode_component (s)
  return (s:gsub("[^A-Za-z0-9%-._~]", encode))
end

return class
