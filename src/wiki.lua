local vaporoid = require("vaporoid")

local prologue_tmpl = vaporoid.tempo.compile([=[
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>[% title | escape_html %]</title>
    <link rel="stylesheet" href="/wiki-lib/bootstrap/css/bootstrap.css">
[% IF navbar %]
    <style>body{padding-top:60px}</style>
[% ELSE %]
    <style>body{padding-top:20px}</style>
[% END %]
    <link rel="stylesheet" href="/wiki-lib/bootstrap/css/bootstrap-responsive.css">
    <style>
      .mincho {
        font-family: "Hiragino Mincho ProN", serif;
      }
      .gothic {
        font-family: "Hiragino Kaku Gothic ProN", sans-serif;
      }
      .vertical {
        overflow: auto;
        -webkit-writing-mode: vertical-rl;
      }
      .box20x20 {
        width: 20em;
        height: 20em;
      }
      .box40x40 {
        width: 40em;
        height: 40em;
      }
    </style>
  </head>
  <body>
[% IF navbar %]
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="/wiki/">[% brand | escape_html %]</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="./[% new_title | encode_uri %]?q=edit"><i class="icon-edit"></i> [% menu_new | escape_html %]</a></li>
              <li><a href="/wiki/[% title | encode_uri %]?q=list"><i class="icon-list"></i> [% menu_list | escape_html %]</a></li>
              <li><a href="/wiki/[% title | encode_uri %]"><i class="icon-file"></i> [% menu_view | escape_html %]</a></li>
              <li><a href="/wiki/[% title | encode_uri %]?q=edit"><i class="icon-pencil"></i> [% menu_edit | escape_html %]</a></li>
[% IF mobile_safari %]
              <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=mincho"><i class="icon-font"></i> [% menu_mincho | escape_html %]</a></li>
              <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=gothic"><i class="icon-font"></i> [% menu_gothic | escape_html %]</a></li>
              <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=vertical%20mincho%20box20x20"><i class="icon-font"></i> [% menu_vertical20 | escape_html %]</a></li>
              <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=vertical%20mincho%20box40x40"><i class="icon-font"></i> [% menu_vertical40 | escape_html %]</a></li>
[% ELSE %]
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-font"></i> [% menu_style | escape_html %]<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=mincho"><i class="icon-font"></i> [% menu_mincho | escape_html %]</a></li>
                  <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=gothic"><i class="icon-font"></i> [% menu_gothic | escape_html %]</a></li>
                  <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=vertical%20mincho%20box20x20"><i class="icon-font"></i> [% menu_vertical20 | escape_html %]</a></li>
                  <li><a href="/wiki/[% title | encode_uri %]?q=view&amp;style=vertical%20mincho%20box40x40"><i class="icon-font"></i> [% menu_vertical40 | escape_html %]</a></li>
                </ul>
              </li>
[% END %]
            </ul>
            <form method="GET" action="/wiki/[% title | encode_uri %]" class="navbar-form pull-right">
              <input type="hidden" name="q" value="search">
              <input type="text" class="search-query" name="query" placeholder="[% menu_search | escape_html %]">
              [% space %]<button type="submit" class="btn"><i class="icon-search"></i> [% button_search | escape_html %]</button>
            </form>
          </div>
        </div>
      </div>
    </div>
[% END %]
[% IF style %]
    <div class="container [% style | escape_html %]">
[% ELSE %]
    <div class="container">
[% END %]
]=])

local epilogue_tmpl = vaporoid.tempo.compile([=[
    </div>
    <script src="/wiki-lib/jquery-2.0.2.js"></script>
    <script src="/wiki-lib/bootstrap/js/bootstrap.js"></script>
    <script>
      $(function () {
        $(".dropdown ul li a").click(function(e) {
          console.log(e);
          e.stopPropagation();
        });
      });
    </script>
  </body>
</html>
]=])

local edit_tmpl = vaporoid.tempo.compile([=[
      <form method="POST" action="/wiki/[% title | encode_uri %]">
        <input type="text" class="input-block-level" name="title" value="[% title | escape_html %]">
        <textarea class="input-block-level" name="text" rows="20">[% text | escape_html %]</textarea>
        <button type="submit" class="btn btn-primary">[% button_save | escape_html %]</button>
      </form>
]=])

local error_tmpl = vaporoid.tempo.compile([=[
      <p class="text-error">[% text | escape_html %]</p>
]=])

local resource = {
  space           = " ";
  brand           = "vaporoid-wiki";
  navbar          = true;
  new_title       = "New (%Y-%m-%d %H:%M:%S)";
  menu_new        = "New";
  menu_list       = "List";
  menu_view       = "View";
  menu_edit       = "Edit";
  menu_search     = "Search";
  menu_style      = "Style";
  menu_mincho     = "Mincho";
  menu_gothic     = "Gothic";
  menu_vertical20 = "Vertical 20";
  menu_vertical40 = "Vertical 40";
  button_search   = "Search";
  message_deleted = "Deleted.";
  button_save     = "Save";
}

local function dbd_prepared (r, db, label)
  local sth, message = db:prepared(r, label)
  if message then
    error(message)
  end
  return sth
end

local function dbd_select (sth, ...)
  local result, message = sth:select(...)
  if message then
    error(message)
  end
  return result(0)
end

local function dbd_query (sth, ...)
  local result, message = sth:query(...)
  if message then
    error(message)
  end
  return result
end

function fn (r, env, db)
  local user_agent = r.headers_in["User-Agent"]
  if user_agent:match("AppleWebKit/") and user_agent:match("Mobile/") then
    env.mobile_safari = true
  end

  if r.method == "GET" then
    local query = r:parseargs()

    if query.style then
      env.style = query.style
    end

    if query.q == "edit" then
      local result = dbd_select(dbd_prepared(r, db, "vaporoid_wiki_get"), env.title)
      env.text = result[1] and result[1][2] or ""
      return "edit"
    elseif query.q == "list" then
      local label = env.title:sub(1, 1) == "." and "vaporoid_wiki_list" or "vaporoid_wiki_list_public"
      local result = dbd_select(dbd_prepared(r, db, label))
      local text = {}
      for i, v in pairs(result) do
        table.insert(text, "* [[/wiki/" .. v[1] .. "|/" .. v[1] .. "]]")
      end
      env.text = table.concat(text, "\n")
      return "view"
    elseif query.q == "search" then
      local sql = {}
      table.insert(sql, "SELECT title, text FROM vaporoid_wiki WHERE")
      if env.title:sub(1, 1) == "." then
        table.insert(sql, "1")
      else
        table.insert(sql, "NOT title LIKE '.%'")
      end
      for v in (query.query or ""):gmatch("[^%s]+") do
        local v = '%' .. v:gsub("'", "''"):gsub("[%_$]", "$%1") .. '%'
        table.insert(sql, string.format("AND (title LIKE '%s' OR text LIKE '%s')", v, v))
      end
      table.insert(sql, "ORDER BY title")
      local result, message = db:select(r, table.concat(sql, " "))
      if message then
        error(message)
      end
      local result = result(0)
      local text = {}
      for i, v in pairs(result) do
        table.insert(text, "* [[/wiki/" .. v[1] .. "|/" .. v[1] .. "]]")
      end
      env.text = table.concat(text, "\n")
      return "view"
    elseif query.q == "append" then
      local result = dbd_select(dbd_prepared(r, db, "vaporoid_wiki_get"), env.title)
      env.text = result[1] and result[1][2] or ""
      env.text = env.text .. query.text .. "\n"
      dbd_query(dbd_prepared(r, db, "vaporoid_wiki_put"), env.title, env.text)
      return "view"
    end
  elseif r.method == "POST" then
    local query = r:parsebody(1048576)
    env.title = query.title or ""
    env.text = query.text or ""
    if env.text == "" then
      dbd_query(dbd_prepared(r, db, "vaporoid_wiki_delete"), env.title)
      env.text = env.message_deleted
      return "view"
    else
      dbd_query(dbd_prepared(r, db, "vaporoid_wiki_put"), env.title, env.text)
      return "view"
    end
  end

  local result = dbd_select(dbd_prepared(r, db, "vaporoid_wiki_get"), env.title)
  env.text = result[1] and result[1][2] or ""
  return "view"
end

function handle(r)
  local env = {
    title = r.path_info:match("^/?(.*)");
    escape_html = function (s)
      return vaporoid.html.escape(s)
    end;
    encode_uri = function (s)
      return vaporoid.html.escape(vaporoid.uri.encode(s))
    end;
    encode_uri_component = function (s)
      return vaporoid.uri.encode_component(s);
    end;
  }
  for k, v in pairs(resource) do
    local key = "VAPOROID_WIKI_" .. k:upper()
    env[k] = r.subprocess_env[key] and r.subprocess_env[key] or v
  end
  env.new_title = os.date(env.new_title)

  local db = r:dbacquire("mod_dbd")
  -- db:query(r, "PRAGMA foreign_keys = ON")
  -- db:query(r, "PRAGMA case_sensitive_like = OFF;")
  local result, message = pcall(fn, r, env, db)
  db:close()

  r.status = result and 200 or 500
  r.content_type = "text/html; charset=UTF-8"

  local buffer = vaporoid.writer.array()
  if result then
    if message == "view" then
      vaporoid.wiki.run(buffer, env.text, { r = r, env = env })
    elseif message == "edit" then
      vaporoid.tempo.run(edit_tmpl, buffer, env)
    end
  else
    r:err(message)
    env.text = message
    vaporoid.tempo.run(error_tmpl, buffer, env)
  end

  local out = vaporoid.writer.apache(r)
  vaporoid.tempo.run(prologue_tmpl, out, env)
  for i, v in ipairs(buffer.out) do
    out:write(v)
  end
  vaporoid.tempo.run(epilogue_tmpl, out, env)
end
