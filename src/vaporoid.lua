return {
  colm   = require("vaporoid.colm");
  html   = require("vaporoid.html");
  tempo  = require("vaporoid.tempo");
  uri    = require("vaporoid.uri");
  wiki   = require("vaporoid.wiki");
  writer = require("vaporoid.writer");
}
