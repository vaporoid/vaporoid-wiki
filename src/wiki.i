%module vaporoid_wiki
%rename("%(regex:/(.*)/vaporoid_wiki_\\1/)s", %$isclass) "";

%{
#include <colm/colm.h>
#include <vaporoid/colm.hpp>
#include <wiki.hpp>
%}

%include std_string.i

%inline %{
inline std::string prefix() {
  return "vaporoid_wiki_";
}
%}

%include std_vector.i

namespace std {
  %template(vaporoid_wiki_vector_string) vector<string>;
}

%include colm/colm.h
%include vaporoid/colm.hpp
%include wiki.hpp
