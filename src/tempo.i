%module vaporoid_tempo
%rename("%(regex:/(.*)/vaporoid_tempo_\\1/)s",  %$isclass) "";

%{
#include <colm/colm.h>
#include <vaporoid/colm.hpp>
#include <tempo.hpp>
%}

%include std_string.i

%inline %{
inline std::string prefix() {
  return "vaporoid_tempo_";
}
%}

%include std_vector.i

namespace std {
  %template(vaporoid_tempo_vector_string) vector<string>;
}

%include colm/colm.h
%include vaporoid/colm.hpp
%include tempo.hpp
