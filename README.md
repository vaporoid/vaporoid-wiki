# build

    mkdir -p build
    cd build
    env CC=gcc CXX=g++ cmake -DCMAKE_CXX_FLAGS="-Wall -W -std=c++11" -DCMAKE_BUILD_TYPE=release -DCMAKE_INSTALL_PREFIX=/opt/vaporoid/package-gcc/prefix ..
    make
    make test
    make install

# table

    CREATE TABLE IF NOT EXISTS vaporoid_wiki (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT NOT NULL UNIQUE, text TEXT NOT NULL)

# httpd.conf

    LoadModule dbd_module libexec/mod_dbd.so
    LoadModule lua_module libexec/mod_lua.so
    LoadModule proxy_module libexec/mod_proxy.so
    LoadModule proxy_http_module libexec/mod_proxy_http.so
    LoadModule rewrite_module libexec/mod_rewrite.so
    
    <Directory /.../prefix/share/vaporoid-wiki>
      AllowOverride None
      Options None
    
      LuaScope thread
    
      SetEnv VAPOROID_WIKI_BRAND           "煙人計画"
      SetEnv VAPOROID_WIKI_NEW_TITLE       "名づけられていないテキスト (%Y-%m-%d %H:%M:%S)"
      SetEnv VAPOROID_WIKI_MENU_NEW        "新規"
      SetEnv VAPOROID_WIKI_MENU_LIST       "一覧"
      SetEnv VAPOROID_WIKI_MENU_VIEW       "表示"
      SetEnv VAPOROID_WIKI_MENU_EDIT       "編集"
      SetEnv VAPOROID_WIKI_MENU_SEARCH     "キーワード"
      SetEnv VAPOROID_WIKI_MENU_MINCHO     "明朝"
      SetEnv VAPOROID_WIKI_MENU_GOTHIC     "ゴシック"
      SetEnv VAPOROID_WIKI_MENU_VERTICAL20 "縦書き (20x20)"
      SetEnv VAPOROID_WIKI_MENU_VERTICAL40 "縦書き (40x40)"
      SetENV VAPOROID_WIKI_BUTTON_SEARCH   "検索"
      SetEnv VAPOROID_WIKI_MESSAGE_DELETED "削除しました。"
      SetENV VAPOROID_WIKI_BUTTON_SAVE     "保存"
    </Directory>
    
    DBDriver sqlite3
    DBDParams "/.../dbd.db"
    DBDPrepareSQL "SELECT title, text FROM vaporoid_wiki                           ORDER BY title" vaporoid_wiki_list
    DBDPrepareSQL "SELECT title, text FROM vaporoid_wiki WHERE NOT title LIKE '.%' ORDER BY title" vaporoid_wiki_list_public
    DBDPrepareSQL "SELECT title, text FROM vaporoid_wiki WHERE title = %s"                         vaporoid_wiki_get
    DBDPrepareSQL "REPLACE INTO vaporoid_wiki (title, text) VALUES (%s, %s)"                       vaporoid_wiki_put
    DBDPrepareSQL "DELETE FROM vaporoid_wiki WHERE title = %s"                                     vaporoid_wiki_delete
    
    RewriteEngine On
    RewriteRule ^/wiki$ /wiki/ [R]
    RewriteRule ^/wiki/(.*)  /.../prefix/share/vaporoid-wiki/wiki.lua/$1 [L,QSA]
    RewriteRule ^/wiki-(.*)  /.../prefix/share/vaporoid-wiki/$1          [L,QSA]
    RewriteRule ^/(wiki\..*) /.../prefix/share/vaporoid-wiki/$1          [L,QSA]
